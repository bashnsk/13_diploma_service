# skillbox-diploma
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p 8080:8080 skillbox/app
   ```

## Как автоматически развернуть приложение:
a) Автоматический вариант для Ubuntu:
   ```shell
   ./ansible_deploy.sh 
   ```
b) Вручную:
1. Установить ansible
2. Запустить:
   ```shell
   ansible-playbook -b -i ansible/hosts ansible/deploy-servers.yaml -v
   ```
***
Автоматически будет развернуты серверы, для **ci/cd** и **prod**.

С веток разработки ***(кроме uat и main)***, при коммите, автоматически собирается приложение и становится доступным в поддомене **dev**:
[dev](http://dev.surikov.site:8080/)

При успешной сборке и тестировании, ветка **dev** автоматически сливается с веткой пользовательского тестирования - **uat**.

С ветки пользовательсткого тестирования ***(uat)***, автоматически собирается приложение и становится доступным в поддомене **test**:
[test](http://test.surikov.site:8080/)

При успешной сборке и тестировании, ветка **uat** автоматически сливается с веткой продакшена - **main**.
С главной ветки автоматически собирается приложение в **prod**:
[prod](http://surikov.site:8080/)
