#!/bin/bash


if [ "$1" = "--help" ]; then
    echo "Простой скрипт для быстрого развертывания инфраструктуры и запуск приложения(для дипломной работы)."
    echo "Для страта нужен аргумент \"up\"."
    echo "Для сворачивания инфраструктуры (в целях экономии), необходим аргумент \"down\""
exit 0
fi

execute_with_retry() {
    command_to_execute="$1"
    attempt=1
    max_attempts=3

    while [ $attempt -le $max_attempts ]; do
        $command_to_execute
        if [ $? -eq 0 ]; then
            break 
        fi
        sleep 5 
        echo "Ошибка, пробуем еще раз $command_to_execute"
        attempt=$((attempt + 1)) 
        done
}


set -e

if [ $# -eq 1 ]; then
    if [ "$1" = "up" ]; then
            cd ../infra/terraform
            echo "Проверка конфигурации терраформа"
            terraform validate
            echo "Применение конфигурации терраформа"
            execute_with_retry "terraform apply -auto-approve"
            cd ../ansible
            echo "Запуск инфра конфигов"
            execute_with_retry "ansible-playbook -b config-servers.yaml -v"
            cd ../../service/ansible
            echo "Инфраструктура подготовлена к работе."
            echo "Переключаемся на сервисы..."
            sleep 60
            echo "Запуск плейбуков сервиса"
            execute_with_retry "ansible-playbook -b deploy-servers.yaml -v"
            echo Всё запущено и работает.
    elif [ "$1" = "down" ]; then
        echo "Уничтожаем инстансы в облаке"
        cd ../infra/terraform
        execute_with_retry "terraform destroy -auto-approve"
        echo "Инфраструктура удалена"
    else
    echo "Неправильный аргумент! Допустимые значения: 'up' или 'down'"
    fi  
else
echo "Ожидается аргумент: [up|down]"
fi
