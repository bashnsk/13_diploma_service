#!/bin/bash

if [[ "$(uname)" == "Linux" ]]; then
    if [[ $(lsb_release -si) == "Ubuntu" ]]; then
        echo "Detected Ubuntu OS. Installing Ansible..."
        sudo apt-get update
        sudo apt-get install -y ansible
    else
        echo "Unsupported operating system."
        exit 1
    fi
else
    echo "Unsupported operating system."
    exit 1
fi

echo "Running the playbook..."
ansible-playbook -b -i ansible/hosts ansible/deploy-servers.yaml -v && echo "all servers deployed" || echo "while deploying servers, some error occurred."
